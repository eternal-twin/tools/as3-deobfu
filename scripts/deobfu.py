#! /usr/bin/env python3

import collections
import glob
import json
import os
import re
import shutil
import subprocess
import sys

ROOT = os.path.dirname(os.path.dirname(__file__))
TMP_ROOT = os.path.join(ROOT, "tmp")
TMP_SWF_NAME = "tmp"

def get_rabcasm_executable(name):
    if os.name == "nt":
        return os.path.join(ROOT, "rabcdasm", "windows", name + ".exe")
    else:
        return os.path.join(ROOT, "rabcdasm", "linux", name)

def disassemble_swf(path):
    tmp_swf_path = os.path.join(TMP_ROOT, TMP_SWF_NAME + ".swf")
    shutil.copy(path, tmp_swf_path)

    subprocess.check_call([
        get_rabcasm_executable("abcexport"),
        tmp_swf_path
    ])

    asm_dirs = []
    for abc_file in glob.glob(os.path.join(TMP_ROOT, TMP_SWF_NAME + "-*.abc")):
        asm_dir = os.path.splitext(abc_file)[0]
        asm_id = os.path.basename(asm_dir)[(len(TMP_SWF_NAME) + 1):]
        asm_dirs.append((asm_dir, asm_id))
        subprocess.check_call([
            get_rabcasm_executable("rabcdasm"),
            os.path.join(ROOT, abc_file)
        ])

    return asm_dirs

def reassemble_swf(asm_dirs):
    for asm_dir, asm_id in asm_dirs:
        subprocess.check_call([
            get_rabcasm_executable("rabcasm"),
            os.path.join(asm_dir,  TMP_SWF_NAME + "-" + asm_id + ".main.asasm")
        ])
        subprocess.check_call([
            get_rabcasm_executable("abcreplace"),
            os.path.join(TMP_ROOT, TMP_SWF_NAME + ".swf"),
            asm_id,
            os.path.join(asm_dir,  TMP_SWF_NAME + "-" + asm_id + ".main.abc")
        ])

LIT_X_ESCAPE_REGEX = re.compile(r"\\x([0-9A-Za-z]{2})")
LIT_CHAR_ESCAPE_REGEX = re.compile(r"\\.")
LIT_KNOWN_CHAR_ESCAPES = {
    "\\": r"\\", "x": r"\x", "b": r"\x08", "t": r"\x09",
    "n": r"\n", "f": r"\x0C", "r": r"\r"
}
LIT_KNOWN_CHAR_ESCAPES_REV = {
    "5C": r"\\", "08": r"\b", "09": r"\t", "0A": r"\n",
    "0C": r"\f", "0D": r"\r"
}
def normalize_lit(string):
    def replace_char_escapes(match):
        char = match.group()[1]
        try:
            return LIT_KNOWN_CHAR_ESCAPES[char]
        except KeyError:
            raise Exception("unknown escape : \\" + char + " in string " + string)

    string = LIT_X_ESCAPE_REGEX.sub(lambda match: r"\x" + match.group(1).upper(), string)
    string = string.replace("\\x20", " ")
    string = LIT_CHAR_ESCAPE_REGEX.sub(replace_char_escapes, string)
    return string

FORBIDDEN_CHARS_IN_FILE_NAMES = set(ord(c) for c in ("%", "/", ":", "*", "?", "<", ">", "|", "\x7f"))
def escape_for_file(string):
    chars = []
    i, l = 0, len(string)
    while i < l:
        c = string[i]
        if c == "\\":
            escaped = string[i+1]
            if escaped == "x":
                chars.append("%")
                chars.append(string[i+2:i+4].upper())
                i += 4
            else:
                chars.append("%{:02X}".format(ord(escaped)))
                i += 2
        else:
            cp = ord(c)
            if cp in FORBIDDEN_CHARS_IN_FILE_NAMES:
                chars.append("%{:02X}".format(cp))
            else:
                chars.append(c)
            i += 1
    return "".join(chars)

def unescape_for_file(string):
    chars = []
    i, l = 0, len(string)
    while i < l:
        c = string[i]
        if c == "%":
            escaped = string[i+1:i+3]
            if escaped in LIT_KNOWN_CHAR_ESCAPES_REV:
                chars.append(LIT_KNOWN_CHAR_ESCAPES_REV[escaped])
            elif escaped == "":
                chars.append("%")
            else:
                cp = int(escaped, base=16)
                if cp in FORBIDDEN_CHARS_IN_FILE_NAMES:
                    chars.append(chr(cp))
                else:
                    chars.append("\\x")
                    chars.append(escaped.upper())
            i += 3
        else:
            chars.append(c)
            i += 1
    return "".join(chars)

def extract_string_lit_spans(text):
    start = 0
    while True:
        start = text.find("\"", start)
        if start < 0:
            return
        end = start
        while True:
            end = text.find("\"", end + 1)
            if end < 0:
                raise Exception("unterminated string in line: " + text)
            slashes = 1
            while text[end - slashes] == "\\":
                slashes += 1
            if slashes % 2 != 0:
                break

        yield (start + 1, end)
        start = end + 1

STRING_LIT_POSTFIXES = ["/instance/init", "/class/init", "/init"]
def replace_string_lits(text, replacer):
    result = []
    last_lit_end = 0
    for (lit_start, lit_end) in extract_string_lit_spans(text):
        result.append(text[last_lit_end:lit_start])

        string = text[lit_start:lit_end]
        postfix = None
        for p in STRING_LIT_POSTFIXES:
            if string.endswith(p):
                postfix = p
                string = string[:-len(p)]
                break
        
        string = ":".join(
            ".".join(
               replacer(s1) for s1 in s2.split(".")
            ) for s2 in string.split(":")
        )
        result.append(string)
        if postfix is not None:
            result.append(postfix)
        last_lit_end = lit_end


    result.append(text[last_lit_end:])
    return "".join(result)

def apply_mappings(src_dir, dest_dir, mappings, seen_set):
    def map_string(s):
        if s in mappings:
            seen_set.add(s)
            return mappings[s]
        return s

    def map_file_name(s):
        return "/".join(escape_for_file(map_string(unescape_for_file(p))) for p in s.split("/"))

    os.mkdir(dest_dir)
    _, dirs, files = next(os.walk(src_dir))

    for fname in files:
        name = ".".join(map_file_name(f) for f in fname.split("."))
        with open(os.path.join(src_dir, fname)) as src, \
             open(os.path.join(dest_dir, name), "w") as dest:
            for line in src:
                if "#include " in line:
                    line = replace_string_lits(line, map_file_name)
                else:
                    line = replace_string_lits(line, map_string)
                dest.write(line)

    for dname in dirs:
        name = ".".join(map_file_name(d) for d in dname.split("."))
        apply_mappings(os.path.join(src_dir, dname), os.path.join(dest_dir, name), mappings, seen_set)

WHITESPACE_REGEX = re.compile(r"\s+")
COMMENT_REGEX = re.compile(r"^#\s.*|\s#\s.*")
def load_mappings(path, reverse=False):
    mappings = dict()
    mappings_rev = dict()
    is_clear_first = None
    with open(path) as f:
        for line in f:
            orig_line = line
            line = re.sub(COMMENT_REGEX, "", line).strip()

            if not line:
                continue

            parts = WHITESPACE_REGEX.split(line, 2)

            if len(parts) != 2:
                if line == "---clear-first---":
                    is_clear_first = True
                elif line == "---obfu-first---":
                    is_clear_first = False
                else:
                    raise Exception("invalid mapping line: " + orig_line)
                continue

            if is_clear_first is None:
                print("WARNING: order not specified, defaulting to ---obfu-first---")
                is_clear_first = False

            key = normalize_lit(parts[0])
            value = normalize_lit(parts[1])

            if is_clear_first:
                key, value = value, key

            if key in mappings:
                print("WARNING: duplicate key " + key)
            if value in mappings_rev:
                print("WARNING: duplicate value " + value)

            mappings[key] = value
            mappings_rev[value] = key

    return mappings_rev if reverse else mappings

def clean_tmp(path):
    shutil.rmtree(path, True)
    os.makedirs(path)

def parse_args(args):
    if len(args) != 3:
        raise Exception("Wrong number of arguments")
    _, swf, mappings = args
    return swf, mappings

def main():
    swf_path, mappings_path = parse_args(sys.argv)
    mappings = load_mappings(mappings_path)
    seen_set = set()

    clean_tmp(TMP_ROOT)

    asm_dirs = disassemble_swf(swf_path)
    asm_dirs_deobfu = []
    for (asm_dir, asm_id) in asm_dirs:
        deobfu = asm_dir + "-deobfu"
        apply_mappings(asm_dir, deobfu, mappings, seen_set)
        asm_dirs_deobfu.append((deobfu, asm_id))

    for obfu, clear in mappings.items():
        if obfu not in seen_set:
            print("NOTICE: unused mapping " + obfu + " -> " + clear)

    reassemble_swf(asm_dirs_deobfu)
    
    tmp_swf = os.path.join(TMP_ROOT, TMP_SWF_NAME + ".swf")
    final_swf = os.path.splitext(swf_path)
    final_swf = final_swf[0] + "-deobfu" + final_swf[1]
    shutil.move(tmp_swf, final_swf)


if __name__ == "__main__":
    main()
